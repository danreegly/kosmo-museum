﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class target_calibration : MonoBehaviour {

    Transform mainCamera;
    Vector3 pr_position = Vector3.zero;
	void Start () {
        mainCamera = Camera.main.transform;

    }
	
	// Update is called once per frame
	void Update () {
        if(Input.touches.Length == 1){
            transform.localPosition += mainCamera.position - pr_position;
        }
        if (Input.touches.Length == 2)
        {
            Vector3 rot = new Vector3(mainCamera.position.y - pr_position.y, mainCamera.position.x - pr_position.x, 0f);
            rot *= 180;
            transform.localEulerAngles += rot;
        }
        pr_position = mainCamera.position;

    }
    private void OnGUI()
    {
        GUI.Box(new Rect(100, 100, 700, 100), transform.localPosition.x + ", " +
                transform.localPosition.y + ", " + transform.localPosition.z + "; " +
               transform.localEulerAngles.x + ", " + transform.localEulerAngles.y + ", " +
                transform.localEulerAngles.z);
    }
}
