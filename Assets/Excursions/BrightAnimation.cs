﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrightAnimation : MonoBehaviour {

    bool step1 = false;
    public float dur1 = 0.3f;
    public float max = 4f;
    public float dur2 = 0.6f;
    public LensFlare flare;

	
	void Update () {
        if(flare.brightness < max && !step1)
        {
            flare.brightness += max/dur1 * Time.deltaTime;
        }
        else {
            if (!step1)
                step1 = true;
            flare.brightness -= max/dur2 * Time.deltaTime;
        }
	}
}
