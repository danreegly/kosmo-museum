﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaAnimation : BaseAnimator{

    public Material material;
    protected override void UpdateValue()
    {
        material.SetColor("_Color", new Color(material.color.r, material.color.g, material.color.b, value * 140f / 255f));
    }
}
