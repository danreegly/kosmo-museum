﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAnimator : MonoBehaviour {
    public float value = 0f;
    public bool _state = false;
    public float duration = 2f;
    virtual public void Show(bool state){
        _state = state;
    }
    private void Start()
    {
        UpdateValue();
    }
    private void Update()
    {
        if(_state && value != 1f)
        {
            value += Time.deltaTime / duration;
            if (value > 1f)
                value = 1f;
            UpdateValue();
        }

        if (!_state && value != 0f)
        {
            value -= Time.deltaTime / duration;
            if (value < 0f)
                value = 0f;
            UpdateValue();
        }
    }
    virtual protected void UpdateValue(){

    }
}
