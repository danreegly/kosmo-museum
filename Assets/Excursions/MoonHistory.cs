﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonHistory : MonoBehaviour {

    float timer = 0;
    bool step1 = false;
    bool step2 = false;
    bool step3 = false;
    bool step4 = false;
    bool step5 = false;
    bool step6 = false;
    bool step7 = false;
    bool step8 = false;
    public ModernMoonAnimator modernMoonAnimator;
    public ModernMoonAnimator earlyMoonAnimator;
    public TextAnimator textAnimator;
    public GlowAnimator glowAnimator1;
    public GlowAnimator glowAnimator2;
    public LavaAnimation lavaAnimation;
    public SeaAnimation seaAnimation;
    public GameObject lava;
    public GameObject fire;
    public GameObject bigAsteroids;
    public GameObject miniAsteroids;
    public GameObject hotMoon;
    public GameObject earlyMoon;
    void Start () {
        modernMoonAnimator.Show(true);
    }
	
	void Update () {
        timer += Time.deltaTime;
        if(timer > 12f && !step1){
            step1 = true;
            textAnimator.Show(true);
        }
        if (timer > 14f && !step2)
        {
            step2 = true;
            glowAnimator1.Show(true);
            glowAnimator2.Show(true);
            modernMoonAnimator.Show(false);
            textAnimator.Show(false);
        }
        if (timer > 33f && !step3)
        {
            step3 = true;
            fire.active = true;
            glowAnimator1.Show(false);
            glowAnimator2.Show(false);
            lavaAnimation.Show(true);
        }

        if (timer > 43f && !step4)
        {
            step4 = true;
            lava.active = false;
            //lavaAnimation.Show(false);

            earlyMoonAnimator.Show(true);
        }
        if (timer > 45f && !step5)
        {
            step5 = true;
            fire.active = false;
            bigAsteroids.active = true;
            lavaAnimation.Show(false);
        }
        if (timer > 57f && !step6)
        {
            step6 = true;
            //earlyMoonAnimator.Show(false);
            modernMoonAnimator.Show(true);
        }
        if (timer > 78f && !step7)
        {
            step7 = true;
            miniAsteroids.active = true;
        }
        if (timer > 95f && !step8)
        {
            step8 = true;
            modernMoonAnimator.Show(false);
            hotMoon.active = false;
            earlyMoon.active = false;
            seaAnimation.Show(true);
        }
    }
}
