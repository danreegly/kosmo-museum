﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModernMoonAnimator : BaseAnimator{

    public Material material;
    protected override void UpdateValue() {
        material.color = new Color(material.color.r, material.color.g, material.color.b, value);
    }
}
