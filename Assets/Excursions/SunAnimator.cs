﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunAnimator : BaseAnimator{

    public MeshRenderer meshRenderer;
    protected override void UpdateValue() {
        Color color = meshRenderer.material.GetColor("_Color");
        meshRenderer.material.SetColor("_Color", new Color(color.r, color.g, color.b, value));
    }
}
