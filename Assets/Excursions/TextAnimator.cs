﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAnimator : BaseAnimator{

    public TextMesh textMesh;
    protected override void UpdateValue() {
        textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, value);
    }
}
